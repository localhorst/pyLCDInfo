#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
    Date of creation:           2022/05/23
    Date of last modification:  2022/10/17
"""

from datetime import datetime
from rpi_lcd import LCD
import netifaces as ni
import subprocess
import time
import shutil
import sys

lcd = LCD(address=0x3f, bus=2, width=16, rows=2, backlight=True)

def get_ip_addr(interfce_name):
    return {
        "ipv4": ni.ifaddresses(interfce_name)[ni.AF_INET][0]['addr'],
        "ipv6": ni.ifaddresses(interfce_name)[ni.AF_INET6][0]['addr'] ,
    }

def get_wireguard_state(config_name):
    try:
        last_handshake_unix_timestamp = subprocess.check_output(f"wg show {config_name} latest-handshakes",
                                             shell=True, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError:
        return False
    return True

def get_disk_usage(disk_path):
    total, used, free = shutil.disk_usage(disk_path)
    return str(format((free // (2**30))  / (total // (2**30)) * 100.0,'.2f')) + '%'
  
def get_uptime():
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
        if (uptime_seconds >= (60.0*60.0*24.0)):
            return str(format(uptime_seconds/(60.0*60.0*24.0),'.2f')) + ' days'
        else:
            return str(format((uptime_seconds/(60.0*60.0)),'.2f')) + ' hours'


    return strftime("%H:%M:%S", gmtime(60*60*24))


def get_system_time():
    return datetime.now().strftime("%H:%M")

def main():

    if len(sys.argv) != 4:
        print("exiting due to few arguments")
        exit(-1)

    local_interface = sys.argv[1]
    wireguard_interface = sys.argv[2]
    disk_path = sys.argv[3]

    #print(get_ip_addr(local_interface))
    #print(get_wireguard_state(wireguard_interface))
    #print(get_disk_usage(disk_path))
    #print(get_uptime())
    #print(get_system_time())

    lcd.clear()
    lcd.text(" Offsite Backup", 1)
    lcd.text(" fckaf.de/FHA", 2)
    time.sleep(2.0)

    state_ok = False

    while(True):
        if state_ok == False:
            lcd.backlight(turn_on=True)

            lcd.clear()
            lcd.text("Local IPv4:", 1)
            lcd.text(get_ip_addr(local_interface)["ipv4"], 2, align='right')
            time.sleep(2.4)
            lcd.clear()
            lcd.text("Local IPv6:", 1)
            lcd.text(get_ip_addr(local_interface)["ipv6"][14:-1], 2, align='right')
            time.sleep(2.4)
            
            lcd.clear()
            lcd.text("Waiting for VPN", 1)

            for loops in range(4):
                for fill in range(14):
                    if loops % 2 == 0: 
                        lcd.text("<>".rjust(fill), 2, align='left')
                    else:
                        lcd.text("<>".ljust(fill), 2, align='right')
                    time.sleep(0.1)
                

            if get_wireguard_state(wireguard_interface):
                state_ok = True
                lcd.clear()
                lcd.text(" VPN  connected ", 1)
                lcd.text(get_system_time().rjust(16-len(get_system_time())), 2, align='left')
                time.sleep(10.0)
        else:
            lcd.backlight(turn_on=False)
     
            lcd.clear()
            lcd.text("Local IPv4:", 1)
            lcd.text(get_ip_addr(local_interface)["ipv4"], 2, align='right')
            time.sleep(5.0)
            lcd.clear()
            lcd.text("Local IPv6:", 1)
            lcd.text(get_ip_addr(local_interface)["ipv6"][14:-1], 2, align='right')
            time.sleep(5.0)
            lcd.clear()
            lcd.text("Free Disk:", 1)
            lcd.text(get_disk_usage(disk_path), 2, align='right')
            time.sleep(5.0)
            lcd.clear()
            lcd.text("System Uptime:", 1)
            lcd.text(get_uptime(), 2, align='right')
            time.sleep(5.0)

            lcd.clear()
            lcd.text(" VPN  Connected ", 1)
            lcd.text(get_system_time().rjust(16-len(get_system_time())), 2, align='left')
            time.sleep(40.0) #sleep a lot to idle cpu

            #check if wireguard is still connected
            state_ok = get_wireguard_state(wireguard_interface)

if __name__ == "__main__":
    main()
