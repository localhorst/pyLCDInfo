# pyLCDInfo

display current linux/network info on a 16x2 LCD via I2C


## Install

`apt-get install i2c-tools pip python3-dev`

`pip3 install rpi_lcd`

search I2C LCD Controller on Bus X (0, 1, 2, 3, ...) and note address of device
`i2cdetect -y X` 

`nano /lib/systemd/system/LCDinfo.service`

```
[Unit]
Description=LCD info
After=syslog.target
After=network.target

[Service]
Type=simple
User=root
Group=root
Restart=on-failure
RestartSec=5s
ExecStart=sudo /usr/bin/nice -n 19 sudo -u root /usr/bin/python3 /root/pyLCDinfo.py eth0 wg0 /mnt/hdd/backups

[Install]
WantedBy=multi-user.target
```

`systemctl daemon-reload`

`systemctl enable /lib/systemd/system/LCDinfo.service`